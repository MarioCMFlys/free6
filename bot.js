const fs = require('fs');
const paths = require('path');

const Discord = require('discord.js');
const PluginLoader = require('./pluginloader.js');
const CommandHandler = require('./commandhandler.js');

const conf = require('./config.json');

var bot = new Discord.Client();
var loader = new PluginLoader();

global.bot = bot;
global.loader = loader;
global.commandHandler = new CommandHandler();

bot.on('ready', () => {
  var pluginPath = paths.join(__dirname, "plugins");
  if(fs.existsSync(pluginPath)){
    fs.readdirSync(pluginPath).forEach(function(file){
      if(file.endsWith('.js')) {
        file = file.split('.').slice(0, -1).join('.');
        console.log(file);
        loader.loadPlugin(file);
      }
    });
  }

  console.info("Ready for world domination");
});

global.fortunes = fortunes = [
  "It is certain",
  "Without a doubt",
  "You may rely on it",
  "It is decidedly so",
  "Most likely",
  "Outlook good",
  "Signs point to yes",
  "Yes",
  "Reply hazy, try again",
  "Concetrate and ask again",
  "Don't count on it",
  "Outlook not so good",
  "Hell nah",
  "Very doubtful",
  "My reply is no",
  "No"
];


bot.login(conf.token);
