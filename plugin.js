class Plugin {
  constructor(name){
    this.name = name;
  }
  /**
   * Called when the plugin shall be loaded
   * @abstract
   */
  load(){

  }
  /**
   * Called when the plugin is being unloaded
   * @abstract
   */
  unload(){

  }
}

module.exports = Plugin;
