const Plugin = require("./plugin.js");

class PluginLoader {
  /**
   * Initialize the PluginLoader
   */
  constructor(){
    this.pluginList = {};
  }
  /**
   * Load a plugin from the plugins directory
   */
  loadPlugin(name){
    console.info("Loading plugin " + name);
    var lpl = require('./plugins/' + name + '.js');
    var inst = new lpl(name);
    inst.load();
    this.pluginList[name] = inst;
    console.info(name + " successfully loaded");
  }
  /**
   * Unload a plugin
   */
  unloadPlugin(name){
    inst = this.pluginList[name];
    inst.unload();
    delete this.pluginList[name];
  }

  /**
   * Get array of plugins
   * @readonly
   */
  get plugins(){
    return this.pluginList;
  }
}

module.exports = PluginLoader;
