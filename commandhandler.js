class CommandHandler {
  /**
   * Initialize the CommandHandler
   */
  constructor(){
    this.commands = [];
  }

  /**
   * Register a command with the CommandHandler
   * @param match Regular expression to check message against
   * @param exec Function which will be ran upon execution
   */
  register(match, exec){
    var item = {};
    item.exp = match;
    item.exec = exec;
    this.commands[this.commands.length] = item;
  }

  handle(message){
    var msg = message.content;
    for(var i = 0; i<this.commands.length; i++){
      let cmd = this.commands[i];
      if(cmd.exp.test(msg)){
        cmd.exec(message);
        return;
      }
    }
    // do nothing on invalid command
  }
}

module.exports = CommandHandler;
