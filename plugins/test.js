Plugin = require('../plugin.js');
const Discord = require('discord.js');

class Test extends Plugin{
  constructor(name){
    super(name);
  }
  load(){
    global.commandHandler.register(/\!loadertest.*/, function(msg){
      msg.channel.send("it works :joy:");
    });

    global.commandHandler.register(/\!8ball.*/, function(msg){
        var embed8ball = new Discord.RichEmbed()
          .setTitle('My sources say...')
          .setFooter('FREE6 Bot')
          .setColor(0x07AEFF)
          .setDescription(`:8ball: ` + fortunes[Math.floor(Math.random() * fortunes.length)]);
          msg.channel.send(embed8ball)
    });

    global.commandHandler.register(/!ping.*/, function(msg){
      var embedPing = new Discord.RichEmbed()
        .setTitle(`Pong!`)
        .setFooter('FREE6 Bot')
        .setColor(0x07AEFF)
        .setDescription(bot.ping + ' ms');
      msg.channel.send(embedPing);
    });

    global.commandHandler.register(/!avatar.*/, function(msg){
      msg.channel.send(msg.author.avatarURL);
      });

      global.commandHandler.register(/!ping.*/, function(msg){
        var embedHelpInfo = new Discord.RichEmbed()
          .setTitle('Ping')
          .setAuthor('Information') //'https://i.imgur.com/pE33Jum.jpg') < - once we get a picture for the bot
          .setColor(0x07AEFF)
          .setDescription('`ping` - Returns your ping')
          .setFooter('FREE6 Bot')

        var embedHelpFun = new Discord.RichEmbed()
          .setAuthor('Fun') //'https://i.imgur.com/pE33Jum.jpg')
          .setColor(0x07AEFF)
          .setFooter('FREE6 Bot')
          .setTitle('8ball')
          .setDescription('`8ball [argument]` - Uses magic to answer your yes/no questions')
          .addField('Avatar', '`avatar` - Returns a photo of your avatar')

        msg.channel.send(embedHelpInfo);
        msg.channel.send(embedHelpFun);
      });

  }
  unload(){

  }
}

module.exports = Test;
